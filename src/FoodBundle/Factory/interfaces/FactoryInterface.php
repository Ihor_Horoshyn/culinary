<?php

namespace FoodBundle\Factory\interfaces;

interface FactoryInterface 
{
    public function create(array $args);
}
