<?php

namespace FoodBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use FoodBundle\Entity\Dish;
use Symfony\Component\HttpFoundation\Request;

class DishesController extends Controller
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function listAllAction()
    {
        $allDishes = $this->entityManager->getRepository(Dish::class)->findAll();

        return $this->render('@Food/Dishes/index.html.twig', [
            'dishes' => $allDishes
        ]);
    }

    public function viewAction(Request $request)
    {
        $dishId = $request->get('id');
        $dish = $this->entityManager->getRepository(Dish::class)->find($dishId);

        return $this->render('@Food/Dishes/view.html.twig', [
            'dish' => $dish
        ]);
    }

    public function editAction(Request $request)
    {
        $dishId = $request->get('id');
        $dish = $this->entityManager->getRepository(Dish::class)->find($dishId);

        return $this->render('@Food/Dishes/edit.html.twig', [
            'dish' => $dish
        ]);
    }

    public function addIngredientAction(Request $request)
    {
        $dishId = $request->get('id');
        $dish = $this->entityManager->getRepository(Dish::class)->find($dishId);

        return $this->render('@Food/Dishes/add-ingredient.html.twig', [
            'dish' => $dish
        ]);
    }
}
